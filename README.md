# Project to Analyse Restaurant Data in Sydney

It is a university assignment on analysing restaurant data in sydney and getting insights out of that.

## Getting started
I started with uploading the required packages.
Followed by uploading data and required .geojson file to the main code file.

## Data Scleaning and Visualization

Matplotlib.pyplot and seaborn were the main packages used to visualize the data.
Next important one was GeoPandas that has been used to visualise the mapping of density of restaurant filtered with different cuisines.

## Modelling
Regression and Classification modeling was used to predict restaurant rating number
Linear Regression along with optimization, classification models like logistic Regression, Decision tree Classifier, Sv Classifier, kNN Classifier 
has been used and compared.

## Project status
User can expect to see all the results, predictions and informations provided in the code file.

